using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    //Check target in range can change into private <> public :  Target mean enemy 
    private Transform target;
    
    [Header("Attributes")]
    
    public float range = 15f;
    //FireRate.
    public float fireRate = 1f;
    public float fireCountdown = 0f;
    
    [Header("Unity Setup Fields")]

    //TagChecker
    public string enemyTag = "Enemy";

    //LookAtEnemy
    public Transform partRotate;
    public float turnSpeed = 10f;

    //Bullet
    public GameObject bulletPrefab;
    public Transform firePoint;
    
    //Animator
    protected Animator m_animator;
    
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("UpdateTarget",0f,0.1f);
        m_animator = GetComponent<Animator>();
    }

    void UpdateTarget() //Find Target
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
            
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
        }
        else
        {
            target = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            if (fireCountdown > 0)
            {
                fireCountdown -= Time.deltaTime;
            }
            m_animator.SetBool("EnemyNearby",false);
            return;
        }

        //Target Lock On
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partRotate.rotation,lookRotation,Time.deltaTime*turnSpeed).eulerAngles;
        partRotate.rotation = Quaternion.Euler(0f,rotation.y,0f);

        if (fireCountdown <= 0f)
        {
            Shoot();
            fireCountdown = 1 / fireRate;  //ถ้ายิ่งเยอะยิ่งเร็ว
        }
        
        fireCountdown -= Time.deltaTime;    //กลับมาแก้ให้มันรีโหลดรอได้    //แก้เพิ่มในบรรทัดที่ 70-72
        m_animator.SetBool("EnemyNearby",true);
        m_animator.SetTrigger("isTrigger");
    }

    void Shoot()
    {
        GameObject bulletGo = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation); //Clone Prefab bullet
        Bullet bullet = bulletGo.GetComponent<Bullet>();
        
        if(bullet != null)
            bullet.Seek(target);
        Debug.Log("SHOOT!");
    }

    private void OnDrawGizmosSelected() //Radius of range can fire
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,range);
    }
}


