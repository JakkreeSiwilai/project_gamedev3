using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Transform target;

    public int BulletDamage = 20;
    public float speedBullet = 50f;

    public GameObject impactEffect;

    public void Seek(Transform _target) //Find or Seek Target this method
    {
        target = _target;
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speedBullet * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }
        
        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target);
    }


    void HitTarget()
    {
        //Destroy Effect
        GameObject effectIns = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);   
        Destroy(effectIns, 2f);
        
        //Destroy object bullet and calculate damage
        Damage(target);
        Destroy(gameObject);
        Debug.Log("WE HIT!!");
    }

    void Damage(Transform enemy)
    {
        Enemy e = enemy.GetComponent<Enemy>();
        if (e != null)
        {
            e.TakeHit(BulletDamage);
        }
    }
}
