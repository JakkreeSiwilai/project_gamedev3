﻿using UnityEngine;

namespace Scenes.Scripts
{
    [RequireComponent(typeof(Enemy))]
    public class PathEnemyBoss : MonoBehaviour
    {
        private Transform _target;
            //private Transform _lookat;
            private int _wayPointIndex = 0;  //First
        
            private float distanceThreshold = 0.1f;
        
            private WayPoint _wayPoint;
            
            private Enemy enemy;
        
            void Start()
            {
                enemy = GetComponent<Enemy>();
                
                _target = WayPoint.Points[0]; //Array 0 at Waypoint Script
               // _lookat = WayPoint.Points[0];
            }
            
            void Update()
            {
                transform.position =
                    Vector3.MoveTowards(transform.position, _target.position, (enemy.moveSpeed*20) * Time.deltaTime);
        
                /*Vector3 relativePos = _target.position - transform.position;
                Quaternion rotation = Quaternion.LookRotation(relativePos,Vector3.up);
                Vector3 v = rotation.ToEulerAngles();
                rotation.eulerAngles = new Vector3(v.x,v.y -90,v.z);*/
                
                //_rotation = rotation.eulerAngles(v.x,v.y + 90,v.z);
                //rotation.eulerAngles = new Vector3(v.x,v.y + 90,v.z);
                //transform.rotation = rotation;
                
                transform.LookAt(_target);
        
                Vector3 dir = _target.position - transform.position;
                Quaternion lookRotation = Quaternion.LookRotation(dir);
                Vector3 rotation = Quaternion.Lerp(transform.rotation,lookRotation,Time.deltaTime).eulerAngles;
                transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        
                if (Vector3.Distance(transform.position, _target.position) < distanceThreshold)
                {
                    GetNextWaypoint();
                }
                //LookAt ผิด
            }
        
            void GetNextWaypoint()
            {
                if (_wayPointIndex >= WayPoint.Points.Length - 1) //The End
                {
                    EndPathMinusLife();
                    return;
                }
                _wayPointIndex++;
                _target = WayPoint.Points[_wayPointIndex];
            }
        
            void EndPathMinusLife()
            {
                PlayerStats.Lifes--;
                Destroy(gameObject);
            }
    }
}