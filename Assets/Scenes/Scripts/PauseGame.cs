using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseGame : MonoBehaviour
{
    public GameObject UIPause;
    // CameraController CC;
    public CameraController CC;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
        {
            TogglePause();
        }
    }

    public void TogglePause()
    {
        UIPause.SetActive(!UIPause.activeSelf);

        if (UIPause.activeSelf)
        {
            Time.timeScale = 0f;
            CC.enabled = false;
        }
        else
        {
            Time.timeScale = 1f;
            CC.enabled = true;
        }
    }

    public void Retry()
    {
        TogglePause();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Menu()
    {
        Debug.Log("Menu Againnnn");
    }
}
