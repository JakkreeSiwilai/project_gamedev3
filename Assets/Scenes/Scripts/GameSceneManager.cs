using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneManager : MonoBehaviour
{
    protected bool IsSceneOptionsLoaded ;
    
        public void LoadScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
            Time.timeScale = 1f;
        }
    
        public void LoadSceneAdditive(string sceneName)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        }
    
        public void LoadOptionScene(string optionSceneName)
        {
            if (!IsSceneOptionsLoaded)
            {
                SceneManager.LoadScene(optionSceneName, LoadSceneMode.Additive);
                IsSceneOptionsLoaded = true;
            }
    
            Debug.Log(IsSceneOptionsLoaded);
        }
    
        public void LoadCurrentScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            Debug.Log("Ah Shit Here we go again");
        }
    
        public void UnloadScene(string sceneName)
        {
            SceneManager.UnloadSceneAsync(sceneName);
        }
    
        public void UnloadOptionScene(string optionSceneName)
        {
            if(!IsSceneOptionsLoaded)
            {
                SceneManager.UnloadSceneAsync(optionSceneName);
                IsSceneOptionsLoaded = false;
            }
            
        }
    
        public void ExitGame()
        {
            Application.Quit();
        }
    
        #region scene Load and Unload Evenets Handler
    
        private void OnEnable()
        {
            SceneManager.sceneUnloaded += SceneUnloadEventHandler;
            SceneManager.sceneLoaded += SceneLoadedEventHandler;
        }
    
        private void DisEnable()
        {
            SceneManager.sceneUnloaded -= SceneUnloadEventHandler;
            SceneManager.sceneLoaded -= SceneLoadedEventHandler;
        }
    
    
        private void SceneUnloadEventHandler(Scene scene)
        {
    
        }
    
        private void SceneLoadedEventHandler(Scene scene,LoadSceneMode mode)
        {
            //if loaded scene is not the Sceneoption, set flag IsOptionLoaded to false
            //
    
            if (scene.name.CompareTo("SceneOptions") != 0)
            {
                IsSceneOptionsLoaded = false;
            }
        }
        #endregion
}
