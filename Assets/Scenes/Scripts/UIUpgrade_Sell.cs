using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class UIUpgrade_Sell : MonoBehaviour
{
    private Node target;

    public TextMeshProUGUI upgradeCost;
    public Button upgradeButton;

    public TextMeshProUGUI sellingCost;

    public SoundEffect SoundEffect;
    
    [FormerlySerializedAs("gameOverUI")] public GameObject UIUpgrade;

    public void SetTurretTarget(Node _target)
    {
        target = _target;

        transform.position = target.GetBuildPosition();

        if (!target.isMaxUpgrade)
        {
            upgradeCost.text = "$ -" + target.turretBlueprint.upgradeCost;
            upgradeButton.interactable = true;
        }
        else
        {
            upgradeCost.text = "MAX"; 
            upgradeButton.interactable = false;
        }

        sellingCost.text = "$ +" + target.turretBlueprint.GetSellAmount();
        
        
        UIUpgrade.SetActive(true);

        /*transform.position = target.GetBuildPosition();*/
    }

    public void HideUI()
    {
        UIUpgrade.SetActive(false);
    }

    public void Upgrade()
    {
        target.UpgradeTurret();
        BuildManager.instance.DeselectedNode();
        //SoundEffect.PlayAudioUpgrade();
    }

    public void Sell()
    {
        target.SellTurret();
        BuildManager.instance.DeselectedNode();
        SoundEffect.PlayAudioSell();
    }
}
