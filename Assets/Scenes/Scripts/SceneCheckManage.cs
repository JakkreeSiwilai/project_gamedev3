using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneCheckManage : MonoBehaviour
{
    public static bool loseending;
    public static bool winner;
    public bool isPopup;
    public SoundEffect SoundEffect;

    public GameObject gameOverUI;
    public GameObject gameWinningUI;
    
    private void Start()
    {
        loseending = false;
        winner = false;
        isPopup = false;
    }

    void Update()
    {
        if (loseending) //Block loop run every frame
            return;

        if (isPopup == false)
        {
            if (PlayerStats.Lifes <= 0)
            {
                EndGame();
                SoundEffect.PlayAudioLose();
            }
            
            if (winner == true)
            {
                GameWinningUI();
                SoundEffect.PlayAudioWin();
            }
        }
    }

    void EndGame()
    {
        loseending = true;
        //gameOverUI.SetActive(true);
        SceneManager.LoadScene("LoseScene", LoadSceneMode.Additive);
        Debug.Log("Game Over");
        isPopup = true;
    }
    
    public void GameWinningUI()
    {
        winner = true;
        //gameWinningUI.SetActive(true);
        SceneManager.LoadScene("WinScene", LoadSceneMode.Additive);
        Debug.Log("Winning");
        isPopup = true;
    }
}
