﻿using UnityEngine;
using TMPro;

namespace Scenes.Scripts
{
    public class Wave : MonoBehaviour
    {
        public TextMeshProUGUI moneyText;

        void Update()
        {
            moneyText.text = "Wave : " + TimeSpawner.WaveRound.ToString();
        }
    }
}