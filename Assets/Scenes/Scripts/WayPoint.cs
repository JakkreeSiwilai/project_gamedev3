using UnityEngine;

public class WayPoint : MonoBehaviour
{
   public static Transform[] Points;

   void Awake()
   {
      Points = new Transform[transform.childCount];
      for (int i = 0; i < Points.Length; i++)
      {
         Points[i] = transform.GetChild(i);
      }
   }

   private void OnDrawGizmos()
   {
      /*
      foreach (Transform t in transform)
      {
         Gizmos.color = Color.blue;
         Gizmos.DrawWireSphere(t.position,1f);
      }
      */

      //Line Check Path WayPoint
      Gizmos.color = Color.yellow;
      for (int i = 0; i < transform.childCount - 1; i++)
      {
         Gizmos.DrawLine(transform.GetChild(i).position,transform.GetChild(i+1).position);
      }
      
   }
}
