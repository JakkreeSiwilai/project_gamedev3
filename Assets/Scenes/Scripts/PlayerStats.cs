using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public static int Money;
    public int StartMoney = 400; //Base
    //public int MoneyRemain;

    public static int Lifes;
    public int startLife = 5; 

    void Start()
    {
        Money = StartMoney;
        Lifes = startLife;
    }

}
