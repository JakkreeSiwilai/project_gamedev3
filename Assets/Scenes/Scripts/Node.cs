using System;
using System.Collections;
using System.Collections.Generic;
using Scenes.Scripts;
using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour
{
    //Enough Money
    public Color hoverColor;
    //NE Money
    public Color notEnoghMoneyColor;
    
    public Vector3 positionOffSet;

    [Header("Turret")]
    public GameObject turret;

    public TurretBlueprint turretBlueprint;

    public bool isMaxUpgrade = false;
        
    private Renderer rend;

    private Color startColor;

    private BuildManager buildManager;

    public SoundEffect SoundEffect;

    void Start()
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;

        buildManager = BuildManager.instance;
    }

    public Vector3 GetBuildPosition()
    {
        return transform.position + positionOffSet;
    }

    void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;
         
        if (turret != null)     //Up
        {
            buildManager.SelectedNode(this);
            return;
        }
        
        if(!buildManager.CandBuild) //Can't built
            return;
        
        BuildTurret(buildManager.GetTurretToBuilt());
    }

    void BuildTurret(TurretBlueprint blueprint)
    {
        turretBlueprint = null;
        //Check Money Failed to Build
        if (PlayerStats.Money < blueprint.cost)
        {
            Debug.Log("Not Enough!!");
            return;
        }

        //Money > Cost
        PlayerStats.Money -= blueprint.cost;
        
        //Built Turret
        GameObject _turret = (GameObject)Instantiate(blueprint.prefab, GetBuildPosition(), Quaternion.identity);
        turret = _turret;

        turretBlueprint = blueprint;

        //Effect Particle Built
        GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5);
        buildManager.DeselectedNode();
        SoundEffect.PlayAudioUpgrade();
        Debug.Log("This Turret F**king Build ");
    }

    public void UpgradeTurret()
    {
        //Check Money Failed to Build
        if (PlayerStats.Money < turretBlueprint.upgradeCost)
        {
            Debug.Log("Not Enough upgrade!!");
            return;
        }

        //Money > Cost
        PlayerStats.Money -= turretBlueprint.upgradeCost;
        
        //Destroy old version
        Destroy(turret);
        
        //Built Turret new one
        GameObject _turret = (GameObject)Instantiate(turretBlueprint.upgradePrefab,GetBuildPosition(), Quaternion.identity);
        turret = _turret;

        //Effect Particle Built
        GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5);

        isMaxUpgrade = true;
        SoundEffect.PlayAudioUpgrade();
        Debug.Log("This Turret F**king upgraded ");
    }

    public void SellTurret()
    {
        PlayerStats.Money += turretBlueprint.GetSellAmount();
        
        GameObject effect = (GameObject)Instantiate(buildManager.sellEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5);
        
        Destroy(turret);
        turretBlueprint = null;
        isMaxUpgrade = false;
    }

    void OnMouseEnter()
    {
        if (EventSystem.current.IsPointerOverGameObject())
                return;
        
        if (!buildManager.CandBuild)
            return;

        if (buildManager.HasMoney)
        {
            rend.material.color = hoverColor;
        }
        else
        {
            rend.material.color = hoverColor;
        }
    }

    void OnMouseExit()
    {
        rend.material.color = startColor;
    }
}
