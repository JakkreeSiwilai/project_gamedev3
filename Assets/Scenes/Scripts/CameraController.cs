using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class CameraController : MonoBehaviour
{
    //Mouse
    public Vector2 _turnMouse;
    public float sensitivity = 0.5f;

    //Speed Move on 2D cartesian
    public float panSpeed = 30f;
    

    void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked; //Remove Mouse Cursor
        //Cursor.lockState = CursorLockMode.None;   //Curious
        //sensitivity = startsensitivity;
    }

    void Update()
    {
        if (SceneCheckManage.loseending)    //Cancel camera
        {
            this.enabled = false;
        }
        
        /*Version K and M
        if (Input.GetKey("w") || Input.mousePosition.y >= Screen.height - panBorderThickness)
        {
            transform.Translate(Vector3.forward * panSpeed * Time.deltaTime);
        }
        if (Input.GetKey("s") || Input.mousePosition.y <= panBorderThickness)
        {
            transform.Translate(Vector3.back * panSpeed * Time.deltaTime);
        }
        if (Input.GetKey("d") || Input.mousePosition.x >= Screen.width - panBorderThickness)
        {
            transform.Translate(Vector3.right * panSpeed * Time.deltaTime);
        }
        if (Input.GetKey("a") || Input.mousePosition.x <=panBorderThickness)
        {
            transform.Translate(Vector3.left * panSpeed * Time.deltaTime);
        }
        */
        
        //Version K only. Version1
        /*if (Input.GetKey("w") )
        {
            transform.Translate(Vector3.forward * panSpeed * Time.deltaTime,Space.World);
        }
        if (Input.GetKey("s") )
        {
            transform.Translate(Vector3.back * panSpeed * Time.deltaTime,Space.World);
        }
        if (Input.GetKey("d") )
        {
            transform.Translate(Vector3.right * panSpeed * Time.deltaTime,Space.World);
        }
        if (Input.GetKey("a") ) 
        {
            transform.Translate(Vector3.left * panSpeed * Time.deltaTime,Space.World);
        }
        */
        
        //Mouse
        if (Input.GetMouseButton(1))
        {
            _turnMouse.x += Input.GetAxis("Mouse X") * sensitivity;
            _turnMouse.y += Input.GetAxis("Mouse Y") * sensitivity;
            transform.localRotation = Quaternion.Euler(-_turnMouse.y, _turnMouse.x, 0);
        }
        
        //Version K only. Version 2
        if (Input.GetKey("q") ) //Up
        {
            transform.Translate(Vector3.up* panSpeed * Time.deltaTime,Space.World);
        }
        if (Input.GetKey("e") ) //Down
        {
            transform.Translate(Vector3.down * panSpeed * Time.deltaTime,Space.World);
        }
        
        if (Input.GetKey("w") ) //Forward
        {
            transform.Translate(Vector3.forward * panSpeed * Time.deltaTime);
        }
        if (Input.GetKey("s") ) //Backward
        {
            transform.Translate(Vector3.back * panSpeed * Time.deltaTime);
        }
        if (Input.GetKey("d") ) //Left
        {
            transform.Translate(Vector3.right * panSpeed * Time.deltaTime);
        }
        if (Input.GetKey("a") ) //Right
        {
            transform.Translate(Vector3.left * panSpeed * Time.deltaTime);
        }
        
    }
}
