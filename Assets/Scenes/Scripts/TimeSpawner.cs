using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
    using UnityEngine.Serialization;

    public class TimeSpawner : MonoBehaviour
{
    
    public enum SpawnState
    {
        SPAWNING,
        WAITING,
        COUNTING,
        ENDING
    };
    

    [System.Serializable]
    public class Wave
    {
        //ข้อมูลที่ใส่เข้าไปและเปลี่ยนแปลงได้ข้างนอกด้วยคำสั่ง Serializable โดยประกอบด้วย Prefab > Transform , name , amount , rateSpwan
        public string name;
        public Transform enemy;  
        public int amount;          
        public float rateSpawn;     
    }

    public Wave[] waves;
    private int nextWave = 0;
    public static int WaveRound = 1;

    public float timeBetweenWaves = 20f;
    public float waveCountDown  ;

    private float searchCountdown = 1f; //Check enemy every 1 sec.
    
    public TextMeshProUGUI waveCountDownText;   //Timer Check เวลาลดลงแสดงผลบนหน้าจอ

    public SpawnState state = SpawnState.COUNTING;

    void Start()
    {
        waveCountDown = timeBetweenWaves;
    }

    void Update()
    {

        if (state != SpawnState.ENDING)
        {
            if (state == SpawnState.WAITING)
            {
                //Check if enemy Still Alive!
                if (!EnemyIsAlive())
                {
                    //Begin a next Wave
                    WaveComplete();
                    //return;
                }
                else
                {
                    return;
                }
            }  
        }
        else
        {
            Debug.Log("ENDING");
            StopAllCoroutines();
        }

        
            
        // เป็น Update แรกที่เข้ามาใช้งานเพราะข้างบนที่เป็น WAITING ตอนแรกจะเป็น COUNTING จึงลงมา ณ ตรงนี้เมื่อ waveCountDown = 0 ก็ Spawn ออกมา
        if (state != SpawnState.ENDING)
        {
            if (waveCountDown <= 0 ) 
            {
                if (state != SpawnState.SPAWNING)
                {
                    //Start Spawning
                    StartCoroutine(SpawnWave(waves[nextWave]));
                    waveCountDownText.gameObject.SetActive(false);                  //SetActive การแสดงผลของตัวเลขเมื่อหมดเวลา
                }
            }
            else 
            {
                waveCountDown -= Time.deltaTime;
                waveCountDown = Mathf.Clamp(waveCountDown, 0f, Mathf.Infinity); //Make Sure not < 0

                if (waveCountDownText.text == null)
                {
                    return;
                }
                waveCountDownText.text = String.Format("{0:00.00}", waveCountDown);    //เวลาดึงมาจาก waveCountDown
                waveCountDownText.gameObject.SetActive(true);
            } 
        }
            

        void WaveComplete()
        {
            Debug.Log("Wave Completed!");

            state = SpawnState.COUNTING;
            waveCountDown = timeBetweenWaves;

            if (waves.Length - 1 != 0)
            {
                if (nextWave + 1 > waves.Length - 1 )
                {
                    //nextWave = 0;    // Reset กลับเป็น 0
                    SceneCheckManage.winner = true;
                    Debug.Log("ALL WAVE COMPLETE!");
                    state = SpawnState.ENDING;
                    //เพิ่มฉากจบ
                }
                else
                {
                    nextWave++;
                    WaveRound = nextWave + 1;
                } 
            }
            else
            {
                SceneCheckManage.winner = true;
                Debug.Log("ONLY ONE MISSION");
                state = SpawnState.ENDING;
            }
            
            
        }
            
        bool EnemyIsAlive() //หลังจาก waveCountdown > Spawn จนเสร็จเข้ามาเช็กก่อนนำมาเข้า Method นี้
        {
            searchCountdown -= Time.deltaTime;

            if (searchCountdown <= 0f)
            {
                searchCountdown = 1f;  
                if (GameObject.FindGameObjectWithTag("Enemy") == null)
                {
                    return false;
                }
            }
            return true;
        }
    }

    IEnumerator SpawnWave(Wave _wave)       //IEnumerator จะเหมือนการหน่วงเวลาต่อการเกิดการกระทำใน Method ผ่าน WaitForSecond ณ ที่ใช้ตอนนี้
    {
        Debug.Log(("Spawning wave :"+_wave.name));
        state = SpawnState.SPAWNING;

        //Spawn
        for (int i = 0; i < _wave.amount; i++)  // Spawn ตาม Amount ทีได้ใส่ไป , Delay ตาม rateSpawn ยิ่งค่ามากยยิ่งช้า
        {
            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds(_wave.rateSpawn);
        }
        
        state = SpawnState.WAITING;
        
        yield break;
        
    }

    void SpawnEnemy(Transform _enemy)
    {
        //Spawn Enemy
        Instantiate(_enemy, transform.position, _enemy.transform.rotation);
        Debug.Log("Spawning Enemy : " + _enemy.name);
    }
}
