using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    [SerializeField] protected SoundSetting m_Sounsetting;

    public Slider m_SliderBGM;
    public Slider m_SliderSFX;

    void Start()
    {
        InitializeVolume();
        m_Sounsetting.MasterVolume = m_SliderBGM.value;
        m_Sounsetting.MasterSFXVolume = m_SliderSFX.value;
    }

    private void InitializeVolume()
    {
        SetBGMVolume(m_Sounsetting.MasterVolume);
        SetSFXVolume(m_Sounsetting.MasterSFXVolume);
    }

    public void SetBGMVolume(float vol)
    {
        m_Sounsetting.AudioMixer.SetFloat(m_Sounsetting.MasterVolumeName, vol);
        m_Sounsetting.MasterVolume = vol;
        m_SliderBGM.value = m_Sounsetting.MasterVolume;
    }

    public void SetSFXVolume(float vol)
    {
        m_Sounsetting.AudioMixer.SetFloat(m_Sounsetting.MasterSFXVolumeName, vol);
        m_Sounsetting.MasterSFXVolume = vol;
        m_SliderSFX.value = m_Sounsetting.MasterSFXVolume;
    }
}
