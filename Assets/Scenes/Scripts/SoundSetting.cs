using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(menuName = "GameDevProject/SoundSettingPreset", fileName = "SoundSettingPreset")]
public class SoundSetting : ScriptableObject
{
    public AudioMixer AudioMixer;

    [Header("MasterVolume")] 
    public string MasterVolumeName = "MasterVolume";
    [Range(-80, 20)] 
    public float MasterVolume;

    [Header("BGMVolume")] 
    public string BGMVolumeName = "BGMVolume";
    [Range(-80, 20)] 
    public float BGMVolume;
    
    [Header("MasterSFXVolume")] 
    public string MasterSFXVolumeName = "MasterSFXVolume";
    [Range(-80, 20)] 
    public float MasterSFXVolume;
    
    [Header("LoseVolume")] 
    public string LoseVolumeName = "LoseVolume";
    [Range(-80, 20)] 
    public float LoseVolume;
    
    [Header("SellVolume")] 
    public string SellVolumeName = "SellVolume";
    [Range(-80, 20)] 
    public float SellVolume;
    
    [Header("UpgradeVolume")] 
    public string UpgradeVolumeName = "UpgradeVolume";
    [Range(-80, 20)] 
    public float UpgradeVolume;
    
    [Header("WinVolume")] 
    public string WinVolumeName = "WinVolume";
    [Range(-80, 20)] 
    public float WinVolume;
    
}
