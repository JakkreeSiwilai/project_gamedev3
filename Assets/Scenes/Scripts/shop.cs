using System;
using System.Collections;
using System.Collections.Generic;
using Scenes.Scripts;
using UnityEngine;
using UnityEngine.Serialization;

public class shop : MonoBehaviour
{
    [FormerlySerializedAs("standardTurret")] public TurretBlueprint Scholar;
    [FormerlySerializedAs("anotherTurret")] public TurretBlueprint ScholarSniper;
    public TurretBlueprint ScholarForeigner;
    
    BuildManager buildManager;
    

    private void Start()
    {
        buildManager = BuildManager.instance;
    }

    public void SelectScholar()    
    {
        Debug.Log("Standard Turret Purchase");
        buildManager.SelectTurretToBuild(Scholar);
    }
    
    public void SelectScholarSniper()
    {
        Debug.Log("Another Turret Purchase");
        buildManager.SelectTurretToBuild(ScholarSniper);
    }
    
    public void SelectScholarForeigner()
    {
        Debug.Log("Another Turret Purchase");
        buildManager.SelectTurretToBuild(ScholarForeigner);
    }
}
