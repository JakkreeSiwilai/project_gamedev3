using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffect : MonoBehaviour
{
    public AudioSource audioLose;
    public AudioSource audioSell;
    public AudioSource audioupgrade;
    public AudioSource audiowin;
    
    public void PlayAudioLose()
    {
        audioLose.Play();
    }

    public void PlayAudioSell()
    {
        audioSell.Play();
    }
    
    public void PlayAudioUpgrade()
    {
        audioupgrade.Play();
    }
    
    public void PlayAudioWin()
    {
        audiowin.Play();
    }
    
}
