using System;
using Unity.VisualScripting;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int hp = 100;

    public float setStarterSpeed = 5f;
    
    [HideInInspector]
    public float moveSpeed;

    public int GondarBountyHunter = 50 ; //Money when kill or die that include go in base

    void Start()
    {
        moveSpeed = setStarterSpeed;
    }

    public void TakeHit(int damage)
    {
        hp -= damage;

        if (hp <= 0)    //Enemy Die if not nor run
        {
            Die();
        }
    }

    void Die()
    {
        PlayerStats.Money += GondarBountyHunter;
        Destroy(gameObject); 
    }
}
