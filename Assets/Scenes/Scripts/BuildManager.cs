using System;
using System.Collections;
using System.Collections.Generic;
using Scenes.Scripts;
using UnityEngine;

public class BuildManager : MonoBehaviour
{
    public static BuildManager instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one in scene!");
            return; 
        }
        instance = this;
        
    }

    public GameObject standardTurretPrefab;
    public GameObject anotherTurretPrefab;

    public GameObject buildEffect;
    public GameObject sellEffect;
    
    private TurretBlueprint turretToBuild;

    private Node _nodeselected;

    public UIUpgrade_Sell nodeUI;
    
    //Node null??
    public bool CandBuild
    {
        get { return turretToBuild != null; }
    }

    //Check money and highlgiht
    public bool HasMoney
    {
        get { return PlayerStats.Money >= turretToBuild.cost; }
    }
    
    public void SelectedNode(Node node)
    {
        if (_nodeselected != null)
        {
            DeselectedNode();
            return;
        }
        
        _nodeselected = node;
        //turretToBuild = null;
        nodeUI.SetTurretTarget(node);
    }

    public void SelectTurretToBuild(TurretBlueprint turret)
    {
        DeselectedNode();
        turretToBuild = turret;
    }

    public void DeselectedNode()
    {
        _nodeselected = null;
        nodeUI.HideUI();
    }

    public TurretBlueprint GetTurretToBuilt()
    {
        return turretToBuild;
    }
}
